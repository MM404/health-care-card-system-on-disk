package health_care_card_system;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import health_care_card_system.data.Hospitalization;
import health_care_card_system.data.PatientRecord;

public class DataManager {

	LinearHashing<PatientRecord> lh = new LinearHashing<PatientRecord>(PatientRecord.class);

	public DataManager() {

	}

	public PatientRecord getPatient(int id) {
		return lh.getRecord(new PatientRecord(id, "", "", new Date()));
	}

	public void startHospitalization(int id, Date date, String diagnosis) {
		PatientRecord rec = lh.getRecord(new PatientRecord(id, "", "", new Date()));
		if (rec == null)
			return;
		rec.addHospitalization(new Hospitalization((byte) 1, date.getTime(), -1, diagnosis));
		lh.saveLastBlock();
	}

	public void endHospitalization(int id, Date date) {
		PatientRecord rec = lh.getRecord(new PatientRecord(id, "", "", new Date()));
		if (rec == null)
			return;
		rec.endHospitalization(date.getTime());
		lh.saveLastBlock();

	}

	public void deletePatient(int id) {
		lh.delete(new PatientRecord(id, "", "", new Date()));

	}

	public void editPatient(int id, String forename, String surname, Date birthday) {
		PatientRecord rec = lh.getRecord(new PatientRecord(id, forename, surname, birthday));
		if (rec == null) {
			return;
		}
		if (!forename.isEmpty())
			rec.setForename(forename);
		if (!surname.isEmpty())
			rec.setSurname(surname);
		if (birthday != null)
			rec.setBirthday(birthday);

		lh.saveLastBlock();

	}

	public void editPatientKey(int deleteKey, Integer insertKey, String forename, String surname, Date date) {
		lh.delete(new PatientRecord(deleteKey, forename, surname, date));
		lh.insertRecord(new PatientRecord(insertKey, forename, surname, date));
	}

	public void createPatient(Integer id, String forename, String surname, Date birth) {
		lh.insertRecord(new PatientRecord(id, forename, surname, birth));
	}
	
	public void toTxtFile() {
		lh.toText();
	}

	public void generate(int records, int hosp) {
		String startDateString = "01/01/2016";
		String endDateString = "01/08/2017";
		Random rand = new Random(10);
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = df.parse(startDateString);
			endDate = df.parse(endDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Date randomDate;

		PatientRecord rec;
		for (int i = 100; i < records + 100; i++) {
			randomDate = new Date(ThreadLocalRandom.current().nextLong(startDate.getTime(), endDate.getTime()));
			rec = new PatientRecord(rand.nextInt(100000) + 100, getSaltString(5), getSaltString(5), randomDate);
			for (int j = 0; j < hosp; j++) {
				rec.addHospitalization(new Hospitalization((byte) 1, randomDate.getTime(),
						randomDate.getTime() + 100000, getSaltString(4)));
			}
			lh.insertRecord(rec);
		}

	}

	private String getSaltString(int length) {
		String SALTCHARS = "abcdefghijklmnopqestuvwxyz";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < length) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			Character ch = SALTCHARS.charAt(index);
			if (salt.length() == 0) {
				ch = Character.toUpperCase(ch);
			}
			salt.append(ch);
		}
		String saltStr = salt.toString();
		return saltStr;

	}

	public void save() {
		lh.save();

	}

}
