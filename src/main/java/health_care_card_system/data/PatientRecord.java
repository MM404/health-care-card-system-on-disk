package health_care_card_system.data;

import java.nio.ByteBuffer;
import java.time.LocalDate;
import java.util.Date;

import health_care_card_system.struct.Record;
import health_care_card_system.util.ByteConverter;

public class PatientRecord extends Record {

	public static final int maxHosp = 100;

	private int id;
	private Hospitalization[] hospitalizations;
	private int hospNumber;
	private String forename;
	private int validRecords;

	private String surname;

	private Date birthday;

	public PatientRecord() {
		super();
		this.hospitalizations = new Hospitalization[maxHosp];
		this.hospNumber = 0;
		this.birthday = new Date();
		validRecords = 0;
	}

	public PatientRecord(int id) {
		super();
		this.hospitalizations = new Hospitalization[maxHosp];
		this.hospNumber = 0;
		this.id = id;
		this.birthday = new Date();
		validRecords = 0;
	}

	public PatientRecord(int id, String forename, String surname, Date birthday) {
		super();
		this.hospitalizations = new Hospitalization[maxHosp];
		this.hospNumber = 0;
		this.id = id;
		this.birthday = birthday;
		this.forename = forename;
		this.surname = surname;
		validRecords = 0;
		setValid(true);

		for (int i = 0; i < hospitalizations.length; i++) {
			hospitalizations[i] = new Hospitalization((byte) 0, 0, 0, "diagnozicka");
		}
	}

	public boolean addHospitalization(Hospitalization h) {
		for (int i = 0; i < hospitalizations.length; i++) {
			if (!hospitalizations[i].isValid()) {
				hospitalizations[i] = h;
				return true;
			}
		}
		return false;
	}

	@Override
	public int getKey() {
		return id;
	}

	@Override
	public void loadBytes(byte[] bytes) {

		ByteBuffer b = ByteBuffer.wrap(bytes);
		b.put((byte) (this.isValid() ? 1 : 0));
		b.putInt(id);
		b.put(ByteConverter.matchLength(forename, 25).getBytes());
		b.put(ByteConverter.matchLength(surname, 25).getBytes());
		b.putLong(birthday.getTime());
		for (int j = 0; j < maxHosp; j++) {
			b.put((byte) (hospitalizations[j].isValid() ? 1 : 0));
			b.putLong(hospitalizations[j].getFrom());
			b.putLong(hospitalizations[j].getTo());
			b.put(ByteConverter.matchLength(hospitalizations[j].getDiag(), 40).getBytes());

		}
	}

	@Override
	public void saveBytes(byte[] bytes) {

		byte[] name = new byte[25];
		byte[] diag = new byte[40];
		ByteBuffer b = ByteBuffer.wrap(bytes);
		if (b.get() == (byte) 0)
			setValid(false);
		else
			setValid(true);

		int i = b.getInt();
		this.id = i;
		b.get(name);
		forename = ByteConverter.toString(name, "UTF-8").trim();
		b.get(name);
		surname = ByteConverter.toString(name, "UTF-8").trim();

		birthday = new Date(b.getLong());

		for (int j = 0; j < maxHosp; j++) {
			hospitalizations[j] = new Hospitalization(b.get(), b.getLong(), b.getLong(), "diag");
			b.get(diag);
			hospitalizations[j].setDiagnosis(ByteConverter.toString(diag, "UTF-8").trim());
		}

	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Patient ID: " + id + " Name: " + forename.trim() + " " + surname.trim());

		for (Hospitalization h : hospitalizations) {
			if (h.isValid()) {
				sb.append("\n");
				if (h.getTo() < 1) {
					sb.append(" -HOSP From:" + new Date(h.getFrom()) + " To: NULL " + " diag: " + h.getDiag());
				}else
				sb.append(" -HOSP From:" + new Date(h.getFrom()) + " To: " + new Date(h.getTo()) + " diag: " + h.getDiag());
			}
		}

		return sb.toString();
	}

	@Override
	public int getByteLength() {
		return 1 + Integer.BYTES + 25 * 2 + Long.BYTES + 100 * (2 * Long.BYTES + 40 + 1);
	}

	public void setForename(String forename) {
		this.forename = forename;

	}

	public void setSurname(String surname) {
		this.surname = surname;

	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;

	}

	public boolean endHospitalization(long l) {
		for (int i = 0; i < hospitalizations.length; i++) {
			if (hospitalizations[i].isValid() && hospitalizations[i].getTo() <= 0) {
				hospitalizations[i].setEnd(l);
				return true;
			}
		}
		return false;

	}
}
