package health_care_card_system.data;

import java.util.Date;

public class Hospitalization {
	private boolean isValid;
	private long from;
	private long to;
	private String diagnosis;

	public Hospitalization(byte b, long from, long to, String diagnosis) {
		this.from = from;
		this.to = to;
		this.diagnosis = diagnosis;
		if (b == (byte) 0)
			setValid(false);
		else
			setValid(true);
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public long getFrom() {
		return from;
	}

	public long getTo() {
		return to;
	}

	public String getDiag() {
		return diagnosis;
	}

	public void setDiagnosis(String string) {
		diagnosis = string;

	}

	public void setEnd(long l) {
		to = l;

	}
}
