package health_care_card_system.data;

public class Patient {

	private int id;
	
	public Patient(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
