package health_care_card_system;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import health_care_card_system.data.PatientRecord;
import health_care_card_system.struct.Block;
import health_care_card_system.struct.Record;
import health_care_card_system.util.ByteConverter;

public class LinearHashing<T extends Record> {

	public static String dataFile = "data.lh";
	public static String overflowFile = "overflow.lh";
	public static String controlFile = "control.lh";
	private int blockSize, oBlockSize, recordSize, initBlockCount, level, splitPointer;
	private double dmax;
	private FileOutputStream controlOut;
	private FileInputStream controlIn;
	private RandomAccessFile data;
	private RandomAccessFile dataOver;
	private int takenSpace;
	private int space;
	private Class<T> clazz;
	private ArrayList<Integer> emptyBlocks = new ArrayList<>();
	private int emptySpace;
	private double dmin;
	private Block<T> lastBlock;
	private RandomAccessFile control;

	public LinearHashing(int blockSize, int oBlockSize, int recordSize, int initialBlockCount, int level,
			int splitPointer, double dmax, double dmin, int space, int takenSpace, int emptySpace) {
		this.blockSize = blockSize;
		this.oBlockSize = oBlockSize;
		this.recordSize = recordSize;
		this.initBlockCount = initialBlockCount;
		this.level = level;
		this.splitPointer = splitPointer;
		this.dmax = dmax;
		this.space = space;
		this.takenSpace = takenSpace;
		this.emptySpace = emptySpace;
		this.dmin = dmin;
	}

	public LinearHashing(Class<T> clazz) {
		this.clazz = clazz;
		try {
			this.data = new RandomAccessFile(dataFile, "rwd");
			this.dataOver = new RandomAccessFile(overflowFile, "rwd");
			this.control = new RandomAccessFile(controlFile, "rwd");
			if (control.length() == 0) {
				control.write(ByteConverter.toByte(5));
				control.write(ByteConverter.toByte(3));
				control.write(ByteConverter
						.toByte(1 + Integer.BYTES + 25 * 2 + Long.BYTES + 100 * (2 * Long.BYTES + 40 + 1)));
				control.write(ByteConverter.toByte(2));
				control.write(ByteConverter.toByte(0));
				control.write(ByteConverter.toByte(0));
				control.write(ByteConverter.toByte(0.9));
				control.write(ByteConverter.toByte(0.75));
				control.write(ByteConverter.toByte(0));
				control.write(ByteConverter.toByte(0));
				control.write(ByteConverter.toByte(0));
				// data.write(ByteConverter.toByte(10), 0, Integer.BYTES);
			}
			loadParams();
			if (data.read(ByteConverter.toByte(2)) == -1) {
				data.seek(0);
				for (int i = 0; i < initBlockCount; i++) {
					fillBlock(i);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void loadParams() {
		byte[] paramInt = new byte[Integer.BYTES];
		byte[] paramDouble = new byte[Double.BYTES];
		try {
			this.control.seek(0);
			this.control.read(paramInt);
			this.blockSize = ByteConverter.toInt(paramInt);
			this.control.read(paramInt);
			this.oBlockSize = ByteConverter.toInt(paramInt);
			this.control.read(paramInt);
			this.recordSize = ByteConverter.toInt(paramInt);
			this.control.read(paramInt);
			this.initBlockCount = ByteConverter.toInt(paramInt);
			this.control.read(paramInt);
			this.level = ByteConverter.toInt(paramInt);
			this.control.read(paramInt);
			this.splitPointer = ByteConverter.toInt(paramInt);
			this.control.read(paramDouble);
			this.dmax = ByteConverter.toDouble(paramDouble);
			this.control.read(paramDouble);
			this.dmin = ByteConverter.toDouble(paramDouble);
			this.control.read(paramInt);
			this.space = ByteConverter.toInt(paramInt);
			this.control.read(paramInt);
			this.takenSpace = ByteConverter.toInt(paramInt);
			this.control.read(paramInt);
			this.emptySpace = ByteConverter.toInt(paramInt);

			while (this.control.getFilePointer() < control.length()) {
				this.control.read(paramInt);
				emptyBlocks.add(new Integer(ByteConverter.toInt(paramInt)));
			}
			emptyBlocks.sort(new Comparator<Integer>() {

				@Override
				public int compare(Integer o1, Integer o2) {
					return Integer.compare(o1.intValue(), o2.intValue());
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(blockSize);
		System.out.println(oBlockSize);
		System.out.println(recordSize);
		System.out.println(initBlockCount);
		System.out.println(level);
		System.out.println(splitPointer);
		System.out.println(dmax);
		System.out.println(dmin);
		System.out.println(space);
		System.out.println(takenSpace);
		System.out.println(emptySpace);

	}

	public T getRecord(T record) {
		// get block number using first function
		int bNum = getBlockNum(record.getKey());
		// if block number is less then split pointer
		// use second function
		if (bNum < splitPointer) {
			bNum = getBlockNum2(record.getKey());
		}
		Block<T> block = new Block<T>(bNum, recordSize, blockSize, oBlockSize, false, data, dataOver, clazz, null);
		T rec = block.getRecord(record.getKey());
		lastBlock = block.getLastBlock();

		return rec;
	}

	public boolean insertRecord(T record) {
		// get block number using first function
		int bNum = getBlockNum(record.getKey());
		// if block number is less then split pointer
		// use second function
		if (bNum < splitPointer) {
			bNum = getBlockNum2(record.getKey());
		}
		// insert record into block

		Block<T> block = new Block<T>(bNum, recordSize, blockSize, oBlockSize, false, data, dataOver, clazz, null);
		int overflowCount = block.getOverflowCount();
		int validRecords = block.getRecordCount();
		if (block.insert(record, emptyBlocks, true)) {
			takenSpace++;

			if (overflowCount < block.getOverflowCount()) {
				space += oBlockSize;
			}
			if (validRecords < block.getRecordCount()) {
				emptySpace--;
			}
		} else {
			return false;
		}
		// check if split is needed
		// if more than dmax
		// while ((double) takenSpace / (double) space >= dmax) {
		// split();
		// }
		while ((double) takenSpace / (double) emptySpace >= dmax) {
			split();
		}
		return true;
	}

	public boolean delete(T record) {
		// get block number using first function
		int bNum = getBlockNum(record.getKey());
		// if block number is less then split pointer
		// use second function
		if (bNum < splitPointer) {
			bNum = getBlockNum2(record.getKey());
		}

		// delete
		Block<T> block = new Block<T>(bNum, recordSize, blockSize, oBlockSize, false, data, dataOver, clazz, null);
		int validRecords = block.getRecordCount();
		int ofCount = block.getOverflowCount();
		if (block.deleteRecord(record.getKey(), emptyBlocks)) {
			takenSpace--;
			emptySpace += validRecords - block.getRecordCount();
			space -= ofCount - block.getOverflowCount();
			// check if merge is needed
			while ((double) takenSpace / (double) emptySpace <= dmin && (splitPointer != 0 || level != 0)) {
				merge();
			}
		}

		return false;

	}

	public void merge() {
		int toDeleteNum, mergeNum;
		if (splitPointer == 0) {
			mergeNum = initBlockCount * (int) Math.pow(2, level - 1) - 1;
		} else {

			mergeNum = splitPointer - 1;
		}
		toDeleteNum = splitPointer + initBlockCount * (int) Math.pow(2, level) - 1;

		Block<T> mergeBlock = new Block<T>(mergeNum, recordSize, blockSize, oBlockSize, false, data, dataOver, clazz,
				null);
		Block<T> toDeleteBlock = new Block<T>(toDeleteNum, recordSize, blockSize, oBlockSize, false, data, dataOver,
				clazz, null);

		int validM = mergeBlock.getRecordCount();
		int ofCount = mergeBlock.getOverflowCount();

		emptySpace -= blockSize - toDeleteBlock.getRecordCount();

		for (T rec : toDeleteBlock.getAllRecords()) {
			if (rec.isValid()) {
				mergeBlock.insert(rec, emptyBlocks, false);
			}
		}
		mergeBlock.saveAll();

		emptySpace += validM - mergeBlock.getRecordCount();
		space -= blockSize;
		space -= toDeleteBlock.getOverflowCount() * oBlockSize;
		space += mergeBlock.getOverflowCount() - ofCount;

		toDeleteBlock.clearAllData();
		toDeleteBlock.handleEmptyBlocks(true);

		if (splitPointer == 0) {
			splitPointer = initBlockCount * (int) Math.pow(2, level - 1) - 1;
			level--;
		} else {
			splitPointer--;
		}

		// cut the file
		try {
			data.setLength(toDeleteBlock.getAddress());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void split() {
		int newNum = 0;
		try {
			newNum = (int) data.length() / (blockSize * recordSize + Block.metaLength);
		} catch (IOException e) {

			e.printStackTrace();
		}

		Block<T> splitBlock = new Block<T>(splitPointer, recordSize, blockSize, oBlockSize, false, data, dataOver,
				clazz, null);

		Block<T> newBlock = new Block<T>(newNum, recordSize, blockSize, oBlockSize, false, data, dataOver, clazz, null,
				true);

		space += blockSize;
		int splitBlockRecs = splitBlock.getRecordCount();
		int overflowCount = splitBlock.getOverflowCount();

		ArrayList<T> records = splitBlock.getAllRecords();
		ArrayList<T> recordsToNew = new ArrayList<>();
		splitBlock.clearAllData();
		int bNum = 0;
		for (T rec : records) {
			if (!rec.isValid()) {
				continue;
			}
			bNum = getBlockNum2(rec.getKey());
			if (bNum != splitPointer) {
				recordsToNew.add(rec);
			} else {
				splitBlock.insert(rec, emptyBlocks, false);
			}
		}
		splitBlock.handleEmptyBlocks(false);
		splitBlock.saveAll();
		for (T r : recordsToNew) {
			newBlock.insert(r, emptyBlocks, false);
		}
		newBlock.saveAll();

		emptySpace += (splitBlockRecs - splitBlock.getRecordCount());
		emptySpace += (blockSize - newBlock.getRecordCount());
		space -= (overflowCount - splitBlock.getOverflowCount());

		splitPointer++;
		if (initBlockCount * (int) Math.pow(2.0, Double.valueOf(level)) <= splitPointer) {
			// full expansion
			splitPointer = 0;
			level++;
		} else {
		}

	}

	public void fillBlock(int blockNum) {

		new Block<T>(blockNum, recordSize, blockSize, oBlockSize, false, data, dataOver, clazz, null, true).save();
		space += blockSize;
		emptySpace += blockSize;
	}

	private int getBlockNum(int key) {
		return key % (initBlockCount * (int) Math.pow(2.0, Double.valueOf(level)));
	}

	private int getBlockNum2(int key) {
		return key % (initBlockCount * (int) Math.pow(2.0, Double.valueOf(level + 1)));
	}

	public void printBlock(int blockNum) {
		byte[] meta = new byte[Block.metaLength];
		try {
			data.seek(blockNum * recordSize * blockSize + blockNum * Block.metaLength);
			data.read(meta);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println("--------------------------------------------------------");
		System.out.println("BLCK " + blockNum + "  OVFV: " + ByteConverter.toInt(Arrays.copyOfRange(meta, 0, 4))
				+ " REC: " + ByteConverter.toInt(Arrays.copyOfRange(meta, 4, 8)) + " GREC: "
				+ ByteConverter.toInt(Arrays.copyOfRange(meta, 8, 12)) + " BLCKS: "
				+ ByteConverter.toInt(Arrays.copyOfRange(meta, 12, 16)));
		try {
			for (int i = 0; i < blockSize; i++) {
				data.seek(blockNum * recordSize * blockSize + i * recordSize + (blockNum + 1) * Block.metaLength);
				PatientRecord rec = new PatientRecord();
				byte[] record = new byte[recordSize];
				data.read(record);
				rec.saveBytes(record);
				System.out.println("valid: " + rec.isValid() + " key: " + rec.getKey());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("--------------------------------------------------------");
	}

	public void printBlock(int blockNum, StringBuilder sb) {
		byte[] meta = new byte[Block.metaLength];
		try {
			data.seek(blockNum * recordSize * blockSize + blockNum * Block.metaLength);
			data.read(meta);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		sb.append("--------------------------------------------------------\n");
		sb.append("BLCK " + blockNum + "  OVFV: " + ByteConverter.toInt(Arrays.copyOfRange(meta, 0, 4)) + " REC: "
				+ ByteConverter.toInt(Arrays.copyOfRange(meta, 4, 8)) + " GREC: "
				+ ByteConverter.toInt(Arrays.copyOfRange(meta, 8, 12)) + " BLCKS: "
				+ ByteConverter.toInt(Arrays.copyOfRange(meta, 12, 16)) + "\n");
		try {
			for (int i = 0; i < blockSize; i++) {
				data.seek(blockNum * recordSize * blockSize + i * recordSize + (blockNum + 1) * Block.metaLength);
				PatientRecord rec = new PatientRecord();
				byte[] record = new byte[recordSize];
				data.read(record);
				rec.saveBytes(record);
				sb.append("valid: " + rec.isValid() + " key: " + rec.getKey() + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		sb.append("--------------------------------------------------------\n");
	}

	public void printOverflowBlock(int blockNum) {
		byte[] meta = new byte[Block.metaLength];
		try {
			dataOver.seek(blockNum * recordSize * oBlockSize + blockNum * Block.metaLength);
			dataOver.read(meta);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println("--------------------------------------------------------");
		System.out.println("OF BLCK " + blockNum + "  NEXT: " + ByteConverter.toInt(Arrays.copyOfRange(meta, 0, 4))
				+ " REC: " + ByteConverter.toInt(Arrays.copyOfRange(meta, 4, 8)) + " GREC: "
				+ ByteConverter.toInt(Arrays.copyOfRange(meta, 8, 12)) + " BLCKS: "
				+ ByteConverter.toInt(Arrays.copyOfRange(meta, 12, 16)));
		try {
			for (int i = 0; i < oBlockSize; i++) {
				dataOver.seek(blockNum * recordSize * oBlockSize + i * recordSize + (blockNum + 1) * Block.metaLength);
				PatientRecord rec = new PatientRecord();
				byte[] record = new byte[recordSize];
				dataOver.read(record);
				rec.saveBytes(record);
				System.out.println("valid: " + rec.isValid() + " key: " + rec.getKey());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("--------------------------------------------------------");
	}

	public void printOverflowBlock(int blockNum, StringBuilder sb) {
		byte[] meta = new byte[Block.metaLength];
		try {
			dataOver.seek(blockNum * recordSize * oBlockSize + blockNum * Block.metaLength);
			dataOver.read(meta);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		sb.append("--------------------------------------------------------\n");
		sb.append("OF BLCK " + blockNum + "  NEXT: " + ByteConverter.toInt(Arrays.copyOfRange(meta, 0, 4)) + " REC: "
				+ ByteConverter.toInt(Arrays.copyOfRange(meta, 4, 8)) + " GREC: "
				+ ByteConverter.toInt(Arrays.copyOfRange(meta, 8, 12)) + " BLCKS: "
				+ ByteConverter.toInt(Arrays.copyOfRange(meta, 12, 16)) + "\n");
		try {
			for (int i = 0; i < oBlockSize; i++) {
				dataOver.seek(blockNum * recordSize * oBlockSize + i * recordSize + (blockNum + 1) * Block.metaLength);
				PatientRecord rec = new PatientRecord();
				byte[] record = new byte[recordSize];
				dataOver.read(record);
				rec.saveBytes(record);
				sb.append("valid: " + rec.isValid() + " key: " + rec.getKey() + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		sb.append("--------------------------------------------------------\n");

	}

	public void save() {

		try {
			control.setLength(0);
			control.seek(0);
			control.write(ByteConverter.toByte(blockSize));
			control.write(ByteConverter.toByte(oBlockSize));
			control.write(ByteConverter.toByte(recordSize));
			control.write(ByteConverter.toByte(initBlockCount));
			control.write(ByteConverter.toByte(level));
			control.write(ByteConverter.toByte(splitPointer));
			control.write(ByteConverter.toByte(dmax));
			control.write(ByteConverter.toByte(dmin));
			control.write(ByteConverter.toByte(space));
			control.write(ByteConverter.toByte(takenSpace));
			control.write(ByteConverter.toByte(emptySpace));

			while (this.control.getFilePointer() < control.length()) {
				for (Integer integer : emptyBlocks) {
					control.write(ByteConverter.toByte(integer));
				}
			}

			control.close();
			stats();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

	public void stats() {
		System.out.println("Taken Space: " + takenSpace + ", Empty Space: " + emptySpace + ", Space: " + space);

	}

	public void saveLastBlock() {
		lastBlock.save();
	}

	public void toText() {
		BufferedWriter bw = null;
		FileWriter fw = null;
		StringBuilder sb = new StringBuilder();
		try {

			int i = 0;
			dataOver.seek(0);
			while (dataOver.getFilePointer() < dataOver.length()) {
				printOverflowBlock(i, sb);
				i++;
			}

			fw = new FileWriter("overflow.txt");
			bw = new BufferedWriter(fw);
			bw.write(sb.toString());

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}

		}

		bw = null;
		fw = null;
		sb = new StringBuilder();

		try {

			int i = 0;
			data.seek(0);
			while (data.getFilePointer() < dataOver.length()) {
				printBlock(i, sb);
				i++;
			}
			fw = new FileWriter("data.txt");
			bw = new BufferedWriter(fw);
			bw.write(sb.toString());

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}

		}
	}

}
