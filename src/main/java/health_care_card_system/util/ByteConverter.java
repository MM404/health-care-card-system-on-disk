package health_care_card_system.util;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

public class ByteConverter {

	public static byte[] toByte(int i) {
		return ByteBuffer.allocate(4).putInt(i).array();
	}

	public static byte[] toByte(double d) {
		byte[] bytes = new byte[8];
		ByteBuffer.wrap(bytes).putDouble(d);
		return bytes;
	}

	public static byte[] toByte(long l) {
		return ByteBuffer.allocate(8).putLong(l).array();
	}

	public static byte[] toByte(String s) {
		return s.getBytes();
	}

	public static String toString(byte[] bytes, String encoding) {
		try {
			return new String(bytes, encoding);
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	public static int toInt(byte[] bytes) {
		return ByteBuffer.wrap(bytes).getInt();
	}

	public static long toLong(byte[] bytes) {
		return ByteBuffer.wrap(bytes).getLong();
	}

	public static double toDouble(byte[] bytes) {
		return ByteBuffer.wrap(bytes).getDouble();
	}

	public static String matchLength(String string, int length) {
		StringBuilder sb = new StringBuilder(string);
		if (string.length() > length) {
			sb.setLength(length);
		} else {
			while (sb.length() < length) {
				sb.append(" ");
			}
		}
		return sb.toString();
	}

}
