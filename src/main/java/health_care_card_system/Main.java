package health_care_card_system;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import health_care_card_system.GUI.GUI;
import health_care_card_system.data.PatientRecord;
import javafx.application.Application;

public class Main {

	public static String dataFile = "data.lh";
	public static String overflowFile = "overflow.lh";

	public static void main(String[] args) {
		//test2();
		new GUI();
		Application.launch(GUI.class, args);
	}

	/*
	 * public static void test() {
	 * 
	 * LinearHashing<PatientRecord> dh = new
	 * LinearHashing<PatientRecord>(PatientRecord.class); dh.insertRecord(new
	 * PatientRecord(27, true)); dh.insertRecord(new PatientRecord(18, true));
	 * dh.insertRecord(new PatientRecord(29, true)); dh.insertRecord(new
	 * PatientRecord(28, true)); dh.insertRecord(new PatientRecord(39, true));
	 * dh.insertRecord(new PatientRecord(13, true)); dh.insertRecord(new
	 * PatientRecord(16, true)); dh.insertRecord(new PatientRecord(51, true));
	 * dh.insertRecord(new PatientRecord(19, true)); dh.delete(18); dh.delete(19);
	 * dh.delete(13); dh.delete(28);
	 * 
	 * dh.printBlock(0); dh.printBlock(1); dh.printBlock(2); dh.printBlock(3);
	 * dh.printBlock(4); dh.printOverflowBlock(0); dh.printOverflowBlock(1);
	 * dh.printOverflowBlock(2); dh.printOverflowBlock(3);
	 * 
	 * dh.save(); dh.stats();
	 * 
	 * }
	 */

	private static void test2() {
		final int number = 10000;
		LinearHashing<PatientRecord> dh = new LinearHashing<PatientRecord>(PatientRecord.class);

		Random rand = new Random(150);

		ArrayList<Integer> keys = new ArrayList<>();
		ArrayList<Integer> remainKeys = new ArrayList<>();
		int ra;
		// insert
		for (int i = 0; i < number; i++) {
			ra = rand.nextInt(number * 10000) + 1;
			if (dh.insertRecord(new PatientRecord(ra, "meno", "priezvisko", new Date())))
				;
			keys.add(ra);
		}
		System.out.println("inserted: " + keys.size());

		// get
		int found = 0;
		for (int i : keys) {

			if (dh.getRecord(new PatientRecord(i, null, null, null)) != null) {

				found++;
			} else {
			}
		}
		System.out.println("found: " + found);

		// delete
		System.out.println("---------DELETE----------");
		int remov;

		for (int i = 0; i < number / 3; i++) {
			remov = rand.nextInt(keys.size());
			dh.delete(new PatientRecord(keys.get(remov), "", "", new Date()));
			keys.remove(remov);
		}

		// get
		System.out.println("remaining: " + keys.size());

		found = 0;
		for (int i : keys) {
			if (dh.getRecord(new PatientRecord(i, null, null, null)) != null) {
				found++;
			} else {
			}
		}
		System.out.println("found: " + found);

		// dh.printBlock(0);
		// dh.printBlock(1);
		// dh.printBlock(2);
		// dh.printBlock(3);
		// dh.printBlock(4);
		// dh.printOverflowBlock(0);
		// dh.printOverflowBlock(1);
		// dh.printOverflowBlock(2);
		// dh.printOverflowBlock(3);
	}

}
