package health_care_card_system.GUI;

import java.time.ZoneId;
import java.util.Date;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;

import health_care_card_system.DataManager;
import health_care_card_system.data.PatientRecord;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class MainScreenControl {

	@FXML
	private JFXButton findPatient;
	@FXML
	private JFXButton startHospitalization;
	@FXML
	private JFXButton endHospitalization;
	@FXML
	private JFXButton deletePatient;
	@FXML
	private JFXButton create;
	@FXML
	private JFXButton edit;
	@FXML
	private JFXButton file;
	@FXML
	private JFXButton generate;
	@FXML
	private JFXTextField patienID1;
	@FXML
	private JFXTextField patienID2;
	@FXML
	private JFXTextField patienID3;
	@FXML
	private JFXTextField patienID4;
	@FXML
	private JFXTextField patIDEdit;
	@FXML
	private JFXTextField patToEdit;
	@FXML
	private JFXTextField forename;
	@FXML
	private JFXTextField surname;
	@FXML
	private JFXTextField diagnosis;
	@FXML
	private JFXDatePicker birthday;
	@FXML
	private JFXTextArea textArea;
	private DataManager dataManager;

	public void initialize() {

	}

	public void setDataManager(DataManager dataManager) {
		this.dataManager = dataManager;

	}

	public void findPatient() {
		PatientRecord r = dataManager.getPatient(Integer.valueOf(patienID1.getText()));
		if (r == null) {
			textArea.setText("");
		} else
			textArea.setText(r.toString());
	}

	public void startHospitalization() {
		dataManager.startHospitalization(Integer.valueOf(patienID2.getText()), new Date(), diagnosis.getText());
	}

	public void endHospitalization() {
		dataManager.endHospitalization(Integer.valueOf(patienID3.getText()), new Date());
	}

	public void deletePatient() {
		dataManager.deletePatient(Integer.valueOf(patienID4.getText()));
	}

	public void editPatient() {
		Date date = Date.from(birthday.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
		dataManager.editPatient(Integer.valueOf(patToEdit.getText()), forename.getText(), surname.getText(), date);
	}

	public void editPatientKey() {
		Date date = Date.from(birthday.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
		dataManager.editPatientKey(Integer.valueOf(patToEdit.getText()), Integer.valueOf(patIDEdit.getText()),
				forename.getText(), surname.getText(), date);
	}

	public void createPatient() {
		Date date = Date.from(birthday.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
		dataManager.createPatient(Integer.valueOf(patIDEdit.getText()), forename.getText(), surname.getText(), date);
	}

	public void edit() {
		if (patIDEdit.getText().isEmpty()) {
			editPatient();
		} else {
			editPatientKey();
		}
	}

	public void generate() {
		dataManager.generate(10000, 5);
	}

	public void showFile() {
		dataManager.toTxtFile();
	}
}