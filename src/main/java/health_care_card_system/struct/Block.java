package health_care_card_system.struct;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import health_care_card_system.util.ByteConverter;

public class Block<T extends Record> {

	public static final int metaLength = Integer.BYTES * 4;
	final Class<T> typeParameterClass;
	private int blockNum;
	private int recSize;
	private int blockSize;
	private int oBlockSize;
	private Block<T> nextBlock;
	private byte[] data;
	private int pointer;
	private ArrayList<T> records;
	private boolean isOverflow;
	private RandomAccessFile file;
	private T freeSpace;
	private RandomAccessFile oFile;
	private boolean newOverFlow;
	private Block<T> previousBlock;
	private int validRecords;
	private int validRecordsGroup;
	private int overFlowBlocks;
	private boolean invalid;
	private int blocksWithoutSave = 0;
	private Block<T> lastVisitedBlock;

	public Block(int blockNum, int recSize, int blockSize, int oBlockSize, boolean isOverflow, RandomAccessFile file,
			RandomAccessFile oFile, Class<T> typeParameterClass, Block<T> previousBlock) {
		this.blockNum = blockNum;
		this.recSize = recSize;
		this.blockSize = blockSize;
		this.oBlockSize = oBlockSize;
		this.isOverflow = isOverflow;
		this.typeParameterClass = typeParameterClass;
		records = new ArrayList<>();
		this.file = file;
		this.oFile = oFile;
		this.previousBlock = previousBlock;
		try {
			if (!isOverflow) {
				// file = new RandomAccessFile(DataHandler.dataFile, "rwd");
				file.seek(blockNum * blockSize * recSize);
				data = new byte[recSize * blockSize + metaLength];
			} else {
				// file = new RandomAccessFile(DataHandler.overflowFile, "rwd");
				this.file = oFile;
				file.seek(blockNum * oBlockSize * recSize);
				data = new byte[recSize * oBlockSize + metaLength];
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		load();
	}

	public Block(int blocks, int recSize, int blockSize, int oBlockSize, boolean b, RandomAccessFile file,
			RandomAccessFile oFile, Class<T> typeParameterClass, Block<T> previousBlock, boolean c) {
		this.blockNum = blocks;
		this.recSize = recSize;
		this.blockSize = blockSize;
		this.oBlockSize = oBlockSize;
		this.isOverflow = b;
		this.typeParameterClass = typeParameterClass;
		records = new ArrayList<>();
		this.file = file;
		this.oFile = oFile;
		this.previousBlock = previousBlock;

		try {
			if (!isOverflow) {
				// file = new RandomAccessFile(DataHandler.dataFile, "rwd");
				file.seek(blockNum * blockSize * recSize + (blockNum * metaLength));
				data = new byte[recSize * blockSize + metaLength];
			} else {
				// file = new RandomAccessFile(DataHandler.overflowFile, "rwd");
				this.file = oFile;
				file.seek(blockNum * oBlockSize * recSize + (blockNum * metaLength));
				data = new byte[recSize * oBlockSize + metaLength];
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		allocate();
	}

	public void allocate() {
		try {
			if (!isOverflow) {
				// file = new RandomAccessFile(DataHandler.dataFile, "rwd");
				file.seek(blockNum * blockSize * recSize + (blockNum * metaLength));
				data = new byte[recSize * blockSize + metaLength];

			} else {
				// file = new RandomAccessFile(DataHandler.overflowFile, "rwd");
				file.seek(blockNum * oBlockSize * recSize + (blockNum * metaLength));
				data = new byte[recSize * oBlockSize + metaLength];
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		ByteBuffer bb = ByteBuffer.wrap(data);

		bb.putInt(-1);
		bb.putInt(0);
		bb.putInt(0);
		bb.putInt(0);

		byte[] recordBytes = new byte[recSize];
		bb.put(recordBytes);
		prepare();
		freeSpace = records.get(0);

	}

	private void clearData() {
		try {
			if (!isOverflow) {
				// file = new RandomAccessFile(DataHandler.dataFile, "rwd");
				file.seek(blockNum * blockSize * recSize + (blockNum * metaLength));
				data = new byte[recSize * blockSize + metaLength];
			} else {
				// file = new RandomAccessFile(DataHandler.overflowFile, "rwd");
				file.seek(blockNum * oBlockSize * recSize + (blockNum * metaLength));
				data = new byte[recSize * oBlockSize + metaLength];
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (isOverflow) {
			data = new byte[recSize * oBlockSize + metaLength];
		} else {
			data = new byte[recSize * blockSize + metaLength];
		}

		ByteBuffer bb = ByteBuffer.wrap(data);
		bb.putInt(pointer);
		bb.putInt(0);
		bb.putInt(0);
		bb.putInt(overFlowBlocks);

		byte[] recordBytes = new byte[recSize];
		bb.put(recordBytes);
		prepare();
		freeSpace = records.get(0);

	}

	public void clearAllData() {
		clearData();
		Block<T> block = this.getNextBlock();
		while (block != null) {
			block.clearData();
			block = block.getNextBlock();
		}

	}

	private void load() {
		// System.out.println("LOAD");
		try {
			if (!isOverflow) {
				// file = new RandomAccessFile(DataHandler.dataFile, "rwd");
				file.seek(blockNum * blockSize * recSize + (blockNum * metaLength));
			} else {
				// file = new RandomAccessFile(DataHandler.overflowFile, "rwd");
				file.seek(blockNum * oBlockSize * recSize + (blockNum * metaLength));
			}
			file.read(data);
			prepare();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public boolean insert(T record, ArrayList<Integer> emptyBlocks, boolean save) {
		Record empty = null;
		newOverFlow = false;
		boolean isPrimaryEmpty = false;

		byte[] recordBytes = new byte[recSize];
		for (T rec : records) {

			if (rec.getKey() == record.getKey() && rec.isValid()) {
				return false;
			}
			if (!rec.isValid() && empty == null) {
				empty = rec;
				isPrimaryEmpty = true;
			}

		}

		// if no overflow block
		if (pointer < 0) {
			if (empty != null) {
				record.loadBytes(recordBytes);
				empty.saveBytes(recordBytes);
				validRecords++;
				validRecordsGroup++;
				if (save) {
					save();
				}
			} else {
				overFlowBlocks++;
				validRecordsGroup++;
				this.insertToNewOverFlowBlock(record, emptyBlocks, save);
				if (!save) {
					blocksWithoutSave++;
				}
				return true;
			}

			return true;
		}

		Block<T> block = getNextBlock();
		Block<T> blockToSave = null;
		while (block != null) {
			if (isDuplicate(record)) {
				return false;
			}
			if (empty == null) {
				empty = block.getFreeSpace();
				block.fillFreeSpace();
				blockToSave = block;
			}
			block = block.getNextBlock();

		}

		if (empty != null) {
			record.loadBytes(recordBytes);
			empty.saveBytes(recordBytes);

			if (isPrimaryEmpty) {
				validRecords++;
				validRecordsGroup++;
			} else {
				validRecordsGroup++;
				blockToSave.validRecords++;
				if (save) {
					blockToSave.save();
				}
			}

			if (save) {
				this.save();
			}

		} else {
			newOverFlow = true;
			overFlowBlocks++;
			validRecordsGroup++;
			if (!save) {
				blockToSave.setBlocksWithoutSave(blocksWithoutSave);
			}
			blockToSave.insertToNewOverFlowBlock(record, emptyBlocks, save);
			if (!save) {
				blocksWithoutSave++;
			}
			if (save) {
				save();
			}
		}
		return true;

	}

	private void setBlocksWithoutSave(int i) {
		blocksWithoutSave = i;

	}

	public boolean deleteRecord(int key, ArrayList<Integer> emptyBlocks) {

		for (T r : this.getRecords()) {
			if (r.getKey() == key && r.isValid()) {
				r.setValid(false);
				validRecords--;
				validRecordsGroup--;
				save();
				tidyUpGroup(emptyBlocks);
				return true;
			}
		}
		Block<T> block = this.getNextBlock();
		while (block != null) {
			for (T r : block.getRecords()) {
				if (r.getKey() == key && r.isValid()) {
					r.setValid(false);
					validRecordsGroup--;
					save();
					block.validRecords--;
					block.save();
					tidyUpGroup(emptyBlocks);
					return true;
				}
			}
			block = block.getNextBlock();
		}
		return false;
	}

	private void tidyUpGroup(ArrayList<Integer> emptyBlocks) {
		int allRecords = blockSize + oBlockSize * overFlowBlocks;
		if ((allRecords - validRecordsGroup) >= oBlockSize && this.pointer > -1) {
			ArrayList<T> recs = this.getAllRecords();
			this.clearAllData();
			for (T r : recs) {
				if (r.isValid()) {
					this.insert(r, emptyBlocks, false);
				}

			}
			handleEmptyBlocks(false);
			this.saveAll();
		}
	}

	private boolean isDuplicate(T record) {

		for (T rec : records) {

			if (rec.getKey() == record.getKey() && rec.isValid()) {
				return true;
			}
			if (!rec.isValid()) {
				this.freeSpace = rec;
			}
		}
		return false;
	}

	public T getFreeSpace() {
		return freeSpace;
	}

	public void fillFreeSpace() {
		freeSpace = null;
	}

	public void insertToNewOverFlowBlock(T record, ArrayList<Integer> emptyBlocks, boolean save) {
		try {
			int blocks;
			byte[] recordBytes = new byte[recSize];
			if (!emptyBlocks.isEmpty()) {
				blocks = emptyBlocks.get(0);
				emptyBlocks.remove(0);
			} else
				blocks = (int) ((oFile.length()) / (recSize * oBlockSize + metaLength));
			// System.out.println(blocks+"rec "+record.getKey());
			newOverFlow = true;
			if (!save) {
				blocks += blocksWithoutSave;
			}
			ByteBuffer bb = ByteBuffer.wrap(data);
			bb.put(ByteConverter.toByte(blocks));
			nextBlock = new Block<T>(blocks, recSize, blockSize, oBlockSize, true, file, oFile, typeParameterClass,
					this, true);
			record.loadBytes(recordBytes);
			T rec = nextBlock.getFreeSpace();
			nextBlock.fillFreeSpace();
			rec.saveBytes(recordBytes);
			nextBlock.setPointer(-1);
			nextBlock.validRecords++;
			if (save) {
				nextBlock.save();
			}
			oFile.seek(oFile.length());
			this.pointer = blocks;
			if (save) {
				save();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void saveAll() {
		save();
		Block<T> block = this.getNextBlock();
		while (block != null) {
			block.save();
			block = block.getNextBlock();
		}
	}

	private void setPointer(int i) {
		pointer = i;

	}

	private Block<T> getNextBlock() {
		if (nextBlock == null) {

			if (pointer < 0) {
				return null;
			} else {

				nextBlock = new Block<T>(pointer, recSize, blockSize, oBlockSize, true, oFile, oFile,
						typeParameterClass, this);
				return nextBlock;
			}

		} else
			return nextBlock;
	}

	private void prepare() {
		records.clear();
		pointer = ByteConverter.toInt(Arrays.copyOfRange(data, 0, 4));
		validRecords = ByteConverter.toInt(Arrays.copyOfRange(data, 4, 8));
		validRecordsGroup = ByteConverter.toInt(Arrays.copyOfRange(data, 8, 12));
		overFlowBlocks = ByteConverter.toInt(Arrays.copyOfRange(data, 12, 16));

		int pos = metaLength;
		while (pos < data.length) {
			T rec = (T) newInstance(typeParameterClass);
			rec.saveBytes(Arrays.copyOfRange(data, pos, pos + recSize));
			records.add(rec);
			pos += recSize;
		}

	}

	public void save() {
		// System.out.println("SAVE");
		if (this.invalid) {
			return;
		}
		byte[] block;
		if (isOverflow)
			block = new byte[recSize * oBlockSize + metaLength];
		else
			block = new byte[recSize * blockSize + metaLength];

		byte[] recordBytes = new byte[recSize];
		ByteBuffer bb = ByteBuffer.wrap(block);

		// bb.put(Arrays.copyOfRange(data, 0, metaLength));
		bb.put(ByteConverter.toByte(pointer));
		bb.put(ByteConverter.toByte(validRecords));
		bb.put(ByteConverter.toByte(validRecordsGroup));
		bb.put(ByteConverter.toByte(overFlowBlocks));
		for (T rec : records) {
			rec.loadBytes(recordBytes);
			bb.put(recordBytes);
		}
		try {
			if (isOverflow)
				file.seek(blockNum * oBlockSize * recSize + blockNum * metaLength);
			else
				file.seek(blockNum * blockSize * recSize + blockNum * metaLength);

			file.write(block);
			blocksWithoutSave = 0;

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private <T> T newInstance(Class<T> cls) {
		T myObject = null;
		try {
			myObject = cls.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return myObject;
	}

	private T createRecord(Class<T> clazz) {
		try {
			return clazz.newInstance();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<T> getRecords() {
		return records;
	}

	public ArrayList<T> getAllRecords() {
		ArrayList<T> ret = new ArrayList<>();
		ret.addAll(records);
		Block<T> block = this.getNextBlock();

		while (block != null) {
			ret.addAll(block.getRecords());
			block = block.getNextBlock();
		}

		return ret;
	}

	public boolean hasNewOverFlow() {
		return newOverFlow;
	}

	public ArrayList<Integer> handleEmptyBlocks2(boolean save) {

		ArrayList<Integer> ret = new ArrayList<>();
		Block<T> block = this;

		while (block.getNextBlock() != null) {
			block = block.getNextBlock();
		}

		try {
			long length = oFile.length();
			while (length <= block.getEnd() && block.validRecords == 0 && block.previousBlock != null) {
				length = block.getAddress();
				block.invalid = true;
				block.previousBlock.pointer = -1;
				this.overFlowBlocks--;
				block = block.previousBlock;
				if (save) {
					block.save();
				}
			}

			if (oFile.length() > length) {
				oFile.setLength(length);
			}

			while (block.validRecords == 0 && block.previousBlock != null) {
				ret.add(new Integer(block.blockNum));
				block.previousBlock.pointer = -1;
				this.overFlowBlocks--;
				block = block.previousBlock;
				if (save) {
					block.save();
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return ret;
	}

	public ArrayList<Integer> handleEmptyBlocks(boolean save, ArrayList<Integer> emptyBlocks) {

		ArrayList<Integer> ret = new ArrayList<>();
		Block<T> block = this;

		while (block.getNextBlock() != null) {
			block = block.getNextBlock();
		}

		try {
			long length = oFile.length();
			while (length <= block.getEnd() && block.validRecords == 0 && block.previousBlock != null) {
				length = block.getAddress();
				block.invalid = true;
				block.previousBlock.pointer = -1;
				this.overFlowBlocks--;
				block = block.previousBlock;
				if (save) {
					emptyBlocks.
					block.save();
				}
			}

			if (oFile.length() > length) {
				oFile.setLength(length);
			}

			while (block.validRecords == 0 && block.previousBlock != null) {
				ret.add(new Integer(block.blockNum));
				block.previousBlock.pointer = -1;
				this.overFlowBlocks--;
				block = block.previousBlock;
				if (save) {
					block.save();
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return ret;
	}

	public int getAddress() {
		if (isOverflow) {
			return blockNum * oBlockSize * recSize + blockNum * metaLength;
		} else
			return blockNum * blockSize * recSize + blockNum * metaLength;
	}

	public int getEnd() {
		if (isOverflow) {
			return getAddress() + oBlockSize * recSize + metaLength;
		} else
			return getAddress() + blockSize * recSize + metaLength;
	}

	public int getOverflowCount() {
		return overFlowBlocks;
	}

	public int getRecordCount() {
		return validRecords;
	}

	public T getRecord(int key) {

		Block<T> block = this;

		while (block != null) {
			lastVisitedBlock = block;
			for (T r : block.getRecords()) {
				if (r.getKey() == key && r.isValid()) {
					return r;
				}
			}
			block = block.getNextBlock();
		}
		return null;
	}

	public Block<T> getLastBlock() {
		return lastVisitedBlock;
	}

}
