package health_care_card_system.struct;

public abstract class Record implements Hashable {

	private int length;
	private boolean isValid;

	public Record() {
		isValid = false;
		this.length=getByteLength();
	}

	public long getLength() {
		return length;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
	
}
