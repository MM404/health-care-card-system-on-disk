package health_care_card_system.struct;

public interface Hashable {

	public void loadBytes(byte[] bytes);
	
	public void saveBytes(byte[] bytes);
	
	public int getByteLength();
	
	public int getKey();
	
}
